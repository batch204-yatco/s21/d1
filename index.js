// console.log("Hello, B204!");

/*
	Mini Activity:
		Display ff student number in console. Send screenshot of output in our group chat

		2020-1923
		2020-1924
		2020-1925
		2020-1926
		2020-1927
*/

let idNum = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

console.log(idNum);

/*
	Array
		- used to store multiple related values in a single variable
		- declared using square brackets([]) AKA "Array Literals"

	Syntax:
		let/const arrayName = [elementA, elementB, ..., elementN]
*/

// Common examples of arrays
let grades = [98.5, 94.3, 95, 90];
let computerBrands = ["Acer", "Asus", "HP", "Dell", "Lenovo", "Toshiba", "Fujitsu"];


// Possible to use of array but not recommended
let mixedArr = [12, "Asus", null, undefined, {}];

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
];

console.log(myTasks);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Berlin";

let cities = [city1, city2, city3];
console.log(cities);

// Reassigning array values
console.log("Array before reassignment: ");
console.log(myTasks);
myTasks[0] = "hello world";
console.log("Array after reassignment: ");
console.log(myTasks);

// Array Length property
// .length property allows us to get and set total number of items in an array

console.log(myTasks.length);//4
console.log(cities.length);//3

let blankArr = [];
console.log(blankArr.length);//0

let fullName = "Jonathan Nuestro";
console.log(fullName.length);//16

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// Another example using decrementation
console.log(cities);
cities.length--;
console.log(cities);

// Can we delete a character in string using .length property?
// Cannot do the same on strings
fullName.length = fullName.length - 1;
console.log(fullName.length);
console.log(fullName);


let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// Accessing Elements of an Array
/*
	Syntax:
		arrayName[index];

*/

console.log(grades[3]); //90
console.log(computerBrands[2]); //HP

console.log(computerBrands[20]); //undefined

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("Array before reassignment");
console.log(lakersLegends);

lakersLegends[2] = "Gasol";
console.log("Array after reassignment");
console.log(lakersLegends);

//Accessing last element of array 
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose"];
let lastElementIndex = bullsLegends.length -1;
console.log(bullsLegends[lastElementIndex]); //Rose
console.log(bullsLegends[bullsLegends.length - 1]); //Rose

//Adding Items into Array
let newArr = [];
console.log(newArr[0]); //undefined

newArr[0] = "Rafael Santillan";
console.log(newArr);

newArr[1] = "Lexus Hufano";
console.log(newArr);

newArr[newArr.length] = "Mikki Dela Cerna";
console.log(newArr);

/* Looping over an Array */
for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [49, 14, 30, 12, 22];

for (let index = 0; index < numArr.length; index++){

	console.log(newArr[index]);
}

for (let index = 0; index < numArr.length; index++) {

	if(numArr[index] % 5 === 0) {
		console.log(numArr[index]+" is divisible by 5");
	
	} else {
		console.log(numArr[index]+" is not divisible by 5");
	}
}

// Multidimensional Array
/*

*/

let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
];

console.log(chessBoard);
// Syntax: multiArr[outerArr][innerArray]
console.log(chessBoard[4][4]); //e5
console.log("Pawn moves to: "+ chessBoard[7][4]); //e8